//[SECTION] Dependencies and Modules
const Cart = require('../models/Cart');
const User = require('../models/User');
const Product = require('../models/Product');
const dotenv = require("dotenv"); 

//[SECTION] Environment Setup
dotenv.config();

//[SECTION]FUNCTIONALITY POST

module.exports.userCart = async (req, res) => {
  console.log(req.user.id)

  let prodId = req.body._id
  let proId = req.params.id
  let usrID = req.user.id
  let customerDet = {      
    userId: req.user.id,
    email: req.user.email
  }

  let isCreatedCart =  await Product.findById(prodId).then(foundProduct => {
      console.log(`found product  : ${foundProduct}`)
    let userCart = new Cart({
        userId: req.user.id,
        email: req.user.email,
        // quantity: req.body.quantity,
        // total: req.body.quantity * foundProduct.price,
        product: [{
          productId: foundProduct._id,
          quantity: req.body.quantity,
          totalAmount: req.body.quantity * foundProduct.price
        }]
      })
      
      return userCart.save().then(foundProduct => true, globalThis.getCartId = foundProduct._id).catch(err => err.message)
  
  }) 

  if(isCreatedCart !== true){
    return res.send({message: isCreatedCart})
  } 

  let isUserUpdated = await User.findByIdAndUpdate({_id: req.user.id}).then(userUpdate =>{
        
        let customerCart = {
            cartId: getCartId
        }

        userUpdate.cart.push(customerCart);
        return userUpdate.save().then(userUpdate => true).catch(err => err.message)

  });
    if(isUserUpdated !== true) {
      return res.send({ message: isUserUpdated})
     }   
     
     if(isCreatedCart && isUserUpdated) {
      return res.send({ message: " Successful added to cart."})
    }

}

//[SECTION]FUNCTIONALITY GET
  module.exports.getUserCart= (req, res) => {
    Cart.find({ userId: req.user.id }).then(result => res.send(result)).catch(err => res.send(err))
  }

  
//   module.exports.getAllOrder = (req, res) => {
//     Order.find({}).then(result => res.send(result)).catch(err => res.send(err))
//   }

//   module.exports.getOrder = (req, res) => {
//     let orderid = req.body.orderid
//     Order.findById({orderid}).then(result => res.send(result)).catch(err => res.send(err))
//   }

  
//[SECTION]FUNCTIONALITY PUT


//[SECTION]FUNCTIONALITY DEL

module.exports.deleteCart = (id) => {
 
  return Cart.findByIdAndRemove(id).then((removedProduct, err) => {
    
     if (removedProduct === null){
        return 'No Cart Was Removed, Cart does not exist'
     } else {
        return 'Succesfully removed to cart'
     }
  });
}
