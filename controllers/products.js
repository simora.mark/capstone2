//[SECTION] Dependencies and Modules
    const Product = require('../models/Product');
    const dotenv = require ("dotenv");
const res = require('express/lib/response');

//[SECTION] Environment Setup
    dotenv.config();

//[SECTION] Functionality [CREATE]
module.exports.createProduct = (info) => {

    let productName = info.name;
    let productDescription = info.description;
    let productPrice = info.price;

    let newProduct = new Product({
        name: productName,
        description: productDescription,
        price: productPrice
    });
       
    return Product.findOne({name: productName}).then(product => {
        if(product){
            return (`Product already exists`)
        } else {

           return newProduct.save().then((savedProduct, error) => {
           
                if (error) {
                     
                    return 'Failed to Save New Product';
                } else {
                    
                    return savedProduct; 
                }
            });
        }

    })
    
  };


//[SECTION] Functionality [RETRIEVE]

    module.exports.getAllProducts = () => {
        return Product.find({}).then(outcomeNiFind => {
            return outcomeNiFind
        });
    };

    module.exports.getProduct = (id) => {
        
        return Product.findById(id).then(resultOfQuery => {
            return resultOfQuery;
        });
    };

    module.exports.getAllActiveProducts = () => {
        return Product.find({isActive: true}).then(resultOfTheQuery => {
        return resultOfTheQuery;
    });
    }

    module.exports.getAllArchivedProducts = () => {
        return Product.find({isActive: false}).then(resultOfTheQuery => {
        return resultOfTheQuery;
    });
    }

//[SECTION] Functionality [UPDATE]

module.exports.updateProduct = (id, details) => {
    let pName = details.name; 
    let pDesc = details.description;
    let pCost = details.price;   
    let updatedProduct = {
      name: pName,
      description: pDesc,
      price: pCost
    }
    return Product.findByIdAndUpdate(id, updatedProduct).then((productUpdated, err) => {
        if (err) {
          return {message:'Failed to update Product'}; 
        } else {
          return {message:'Successfully Updated Product'}; 
        }
    })
 }

module.exports.archive = (id) => {
    let updates = {
       isActive:false
    }
    return Product.findByIdAndUpdate(id, updates).then((archived, error) => {
     
        // if(archived === null){
        //     return {message:'Failed to archive product'}; 
        // }else if(archived.isActive === false){
        //     return `The product ${id} "${archived.name}", is already  deactivated`;
        // }else{
        //     return `The product ${id} "${archived.name}" has been deactivated`;
        // }
        if (archived) {
            return `The Product ${id} has been deactivated`;
          } else {
            return 'Failed to archive course'; 
          };

    });
};

 module.exports.activate = (id) => {
   
    let updates = {
       isActive:true
    }
    
    return Product.findByIdAndUpdate(id, updates).then((reactivate, error) => {
     
        if (reactivate === null) {
            return {message:'Failed to activate product, no product found'}
        } else if(reactivate.isActive === true){
            return {message:`The product ${id} "${reactivate.name}" is already activated`};
        } else {
        return {message:`The product ${id} "${reactivate.name}", has been activated successfully`};
        };
        
    });
 };

//[SECTION] Functionality [DELETE]
module.exports.deleteProduct = (id) => {
 
  return Product.findByIdAndRemove(id).then((removedProduct, err) => {
    
     if (removedProduct === null){
        return 'No Product Was Removed, Product does not exist'
     } else {
        return 'Product Succesfully Deleted'
     }
  });
}