//[SECTION] Dependencies and Modules
const User = require('../models/User');
const bcrypt = require ("bcrypt");
const dotenv = require ("dotenv");
const auth = require("../loginAuths");
const req = require('express/lib/request');

//[SECTION] Environment Setup
dotenv.config();
const salt = parseInt(process.env.Salt);

//[SECTION] Functionality [CREATE]
module.exports.createUser = (info) => {
    let Useremail = info.email;
    let Userpassword = info.password;
    const regexPhoneNumber = /^(09|\+639)\d{9}$/;
    let UserMobileno = info.mobileNo;

    let newUser = new User({
        firstName: info.firstName,
        lastName: info.lastName,
        email: info.email,
        password: bcrypt.hashSync(Userpassword, salt),
        mobileNo: UserMobileno
    });
      
    function testMobNum (nstr) {
    return regexPhoneNumber.test(nstr);
    }

    return User.findOne({email: Useremail}).then((user) => {

        if(user){
            return {message: `User already exists`}
        }
        
        if(UserMobileno.match(regexPhoneNumber)){
          return  newUser.save().then((savedUser, error) => {
           
            if (error) {
                return {message: 'Failed to Save New User'};
            } else {
                
                return savedUser; 
            }
        });

        } else {
            return {message:'invalid phone num'};
        }
    }).catch(err => {message:err})
  };


module.exports.loginUser = (req, res) => {
      console.log(req.body)

      User.findOne({email: req.body.email}).then(foundUser => {
  
        if(foundUser === null){
          return res.send("User Not Found");
  
        } else {
  
          const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
  
          console.log(isPasswordCorrect)
          
          if(isPasswordCorrect){
  
            return res.send({accessToken: auth.createAccessToken(foundUser)})
  
          } else {
  
            return res.send("Incorrect Password")
          }
  
        }
  
      }).catch(err => res.send(err))
  };

//!Get User Details

module.exports.getUserDetails = (req, res) => {

    console.log(req.user)

    User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(err => res.send(err))
};

//[SECTION] Functionality [RETRIEVE]
  module.exports.getAllUsers = () => {
      return User.find({}).then(outcomeNiFind => {
      return outcomeNiFind;
      });
  };

  module.exports.getUser = (id) => {
      
      return User.findById(id).then(resultOfQuery => {
          return resultOfQuery;
      });
  };

  module.exports.getAllActiveUsers = () => {
  return User.find({isActive: true}).then(resultOfTheQuery => {
      return resultOfTheQuery;
  });
  }

 
//[SECTION] Functionality [UPDATE]

  module.exports.updateUsertoAdmin = (id, details) => {
          
      let userEmail = details.email;
      let userisAdmin = details.isAdmin;       

      let updateUser = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: userEmail,
        isAdmin: userisAdmin,
        mobileNo: req.body.mobileNo
      }
      return User.findByIdAndUpdate(id, updateUser).then((userUpdated, error) => {
        
        if (error) {
            return 'Failed to update User';
        } else if(userUpdated.isAdmin === true){
            return `${id} ${userUpdated.email}, is already an Admin`
        } else {
            return `Successfully Updated User!`;
        };
      });
  }

  module.exports.updateAdmintoUser = (id, details) => {
          
    let userEmail = details.email;
    let userisAdmin = details.isAdmin;       

    let updateUser = {
      email: userEmail,
      isAdmin: userisAdmin,
    }
    return User.findByIdAndUpdate(id, updateUser).then((userUpdated, error) => {
      
      if (error) {
          return 'Failed to update User';
      } else if(userUpdated.isAdmin === false){
          return `${id} ${userUpdated.email}, is already non admin`
      } else {
          return `Successfully Updated User!`;
      };
    });
}

//[SECTION] Functionality [DELETE]
  module.exports.deleteUser = (id) => {
  
    return User.findByIdAndRemove(id).then((removedUser, err) => {
      
      if (removedUser){
          return 'User Succesfully Deleted'
      } else {
          return 'No User Was Removed'
      }
    });
  }